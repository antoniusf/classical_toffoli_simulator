import pyglet
from math import sqrt, exp, pi, cos, sin
import struct
import itertools

ichain = itertools.chain.from_iterable

false = False
true = True

config = pyglet.gl.Config(sample_buffers=1, samples=4, double_buffer=true)
window = pyglet.window.Window(config=config)
window.set_exclusive_mouse(true)

mousex = mousey = 0

centerx = window.width//2
centery = window.height//2

hover_entity = None

connection_start = None
drag_entity = None
place_new_dr = None
place_new_entities = []

zoom_factor = 1.0
zoom_adjust_mode = False
zoom_adjust_delta = 0
zoom_adjust_factor = 1.0

sens_factor = 1.0
sens_adjust_mode = False
sens_adjust_delta = 0
sens_adjust_factor = 1.0

class Port:

    def __init__ (self, x, y, is_input, shape_type):

        self.x = x
        self.y = y
        self.hover = false
        self.is_input = is_input
        self.connection = None
        self.active = false
        self.shape_type = shape_type

    def mouse_motion (self, dx, dy):

        global hover_entity

        self.hover = false

        if not hover_entity:
            if self.x-3 <= mousex <= self.x+3 and self.y-3 <= mousey <= self.y+3:
                self.hover= true
                hover_entity = self


    def move (self, dx, dy):

        self.x += dx
        self.y += dy


    def mouse_click (self, button):

        global connection_start

        if self.hover:

            if button == pyglet.window.mouse.RIGHT:

                if connection_start == None:

                    if self.connection:
                        self.connection.connection = None
                        self.connection = None

                    connection_start = self

                else:
                    if self.is_input != connection_start.is_input:

                        if self.connection:
                            self.connection.connection = None
                            self.connection = None

                        self.connection = connection_start
                        connection_start.connection = self
                        connection_start = None

            elif button == pyglet.window.mouse.LEFT:
                if self.is_input and not self.connection:
                    self.active = not self.active
                    

    def draw (self):


        x = (self.x-mousex)*zoom_factor + centerx
        y = (self.y-mousey)*zoom_factor + centery
        d2 = 2*zoom_factor
        d3 = 3*zoom_factor
        r = zoom_factor

        if self.connection and self.is_input == true:
            pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                    ('v2f', (x, y, (self.connection.x-mousex)*zoom_factor + centerx, (self.connection.y-mousey)*zoom_factor + centery)),
                    ('c3f', (1., 1., 1.)*2))

        if self.hover:
            color = (.7429, .2392, .3098)
        else:
            if self.active:
                color = (.6078, .9961, .0)
            else:
                color = (.5255, .5529, .4431)

        if self.shape_type == 0:
            pyglet.graphics.draw_indexed(6, pyglet.gl.GL_TRIANGLES, [0, 1, 5, 2, 1, 5, 2, 3, 4, 2, 5, 4],
                    ('v2f', (x+d3, y+d3, x+d3, y-d2, x+d2, y-d3, x-d3, y-d3, x-d3, y+d2, x-d2, y+d3)),
                    ('c3f', color*6))

        elif self.shape_type == 1:
            pyglet.graphics.draw_indexed(6, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5],
                    ('v2f', (x-d3, y+d3, x+d2, y+d3, x+d3, y+d2, x+d3, y-d2, x+d2, y-d3, x-d3, y-d3)),
                    ('c3f', color*6))

        elif self.shape_type == 2:
            pyglet.graphics.draw_indexed(6, pyglet.gl.GL_TRIANGLES, [0, 1, 5, 2, 1, 5, 2, 3, 4, 2, 5, 4],
                    ('v2f', (x-d3, y+d3, x+d2, y+d3, x+d3, y+d2, x+d3, y-d3, x-d2, y-d3, x-d3, y-d2)),
                    ('c3f', color*6))

        elif self.shape_type == 3:
            pyglet.graphics.draw_indexed(6, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5],
                    ('v2f', (x+d3, y+d3, x+d3, y-d3, x-d2, y-d3, x-d3, y-d2, x-d3, y+d2, x-d2, y+d3)),
                    ('c3f', color*6))


        if self.connection:
            if self.active:
                point_color = (.5, .5, .5)
            else:
                point_color = (1., 1., 1.)
                
            points = tuple(ichain((x + r*cos(t*pi/6), y + r*sin(t*pi/6)) for t in range(12)))
            indices = list(ichain((0, i, i+1) for i in range(1, 11)))
            pyglet.graphics.draw_indexed(12, pyglet.gl.GL_TRIANGLES, indices,
                    ('v2f', points),
                    ('c3f', point_color*12))

        
    def update (self):

        if (not self.is_input) and self.connection:
            self.connection.active = self.active

class MouseInput:

    def __init__(self, x, y):

        self.x = x
        self.y = y
        self.out = Port(x+10, y, false, 0)
        self.active = false

    
    def mouse_motion(self, dx, dy):

        global hover_entity

        self.hover = false

        self.out.mouse_motion(dx, dy)

        if not hover_entity:
            if self.x-10 <= mousex <= self.x+10 and self.y-10 <= mousey <= self.y+10:
                self.hover = true
                hover_entity = self


    def set_to (self, x, y):

        self.x = x
        self.y = y
        self.out.x = x+10
        self.out.y = y

    def move (self, dx, dy):

        self.x += dx
        self.y += dy
        self.out.move(dx, dy)


    def mouse_click(self, button):

        global drag_entity

        self.out.mouse_click(button)

        if self.hover:

            if button == pyglet.window.mouse.RIGHT:
                if not drag_entity:
                    drag_entity = self

            elif button == pyglet.window.mouse.LEFT:
                self.active ^= true


    def draw(self):

        x = (self.x-mousex)*zoom_factor + centerx
        y = (self.y-mousey)*zoom_factor + centery
        d10 = 10*zoom_factor

        pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3],
                ('v2f', (x-d10, y-d10, x+d10, y-d10, x+d10, y+d10, x-d10, y+d10)),
                ('c3f', (.9, .5, .3)*4))

        self.out.draw()


    def update(self):

        self.out.active = self.active
        self.out.update()



class Toffoli:

    def __init__(self, x, y):

        self.x = x
        self.y = y
        self.in_a = Port(x-9, y+7, true, 0)
        self.in_b = Port(x-9, y, true, 1)
        self.in_c = Port(x-9, y-7, true, 2)
        self.out_a = Port(x+9, y+7, false, 2)
        self.out_b = Port(x+9, y, false, 3)
        self.out_c = Port(x+9, y-7, false, 0)


    def mouse_motion(self, dx, dy):

        global hover_entity

        self.hover = false

        self.in_a.mouse_motion(dx, dy)
        self.in_b.mouse_motion(dx, dy)
        self.in_c.mouse_motion(dx, dy)
        self.out_a.mouse_motion(dx, dy)
        self.out_b.mouse_motion(dx, dy)
        self.out_c.mouse_motion(dx, dy)

        if not hover_entity:
            if self.x-10 <= mousex <= self.x+10 and self.y-10 <= mousey <= self.y+10:
                self.hover = true
                hover_entity = self


    def move (self, dx, dy):

        self.x += dx
        self.y += dy
        self.in_a.move(dx, dy)
        self.in_b.move(dx, dy)
        self.in_c.move(dx, dy)
        self.out_a.move(dx, dy)
        self.out_b.move(dx, dy)
        self.out_c.move(dx, dy)


    def set_to (self, x, y):

        self.x = x
        self.y = y
        self.in_a.x, self.in_a.y = (x-10, y+7)
        self.in_b.x, self.in_b.y = (x-10, y)
        self.in_c.x, self.in_c.y = (x-10, y-7)
        self.out_a.x, self.out_a.y = (x+10, y+7)
        self.out_b.x, self.out_b.y = (x+10, y)
        self.out_c.x, self.out_c.y = (x+10, y-7)


    def mouse_click(self, button):

        global drag_entity

        self.in_a.mouse_click(button)
        self.in_b.mouse_click(button)
        self.in_c.mouse_click(button)
        self.out_a.mouse_click(button)
        self.out_b.mouse_click(button)
        self.out_c.mouse_click(button)

        if self.hover:
            if not drag_entity:
                drag_entity = self


    def draw(self):

        #pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3],
        #        ('v2f', (self.x-10, self.y-10, self.x+10, self.y-10, self.x+10, self.y+10, self.x-10, self.y+10)),
        #        ('c3f', (.9, .5, .3)*4))

        x = (self.x-mousex)*zoom_factor + centerx
        y = (self.y-mousey)*zoom_factor + centery

        d6 = 6*zoom_factor
        d8 = 8*zoom_factor
        d10 = 10*zoom_factor

        pyglet.graphics.draw_indexed(8, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3, 0, 3, 4, 0, 4, 5, 0, 5, 6, 0, 6, 7],
                ('v2f', (x+d6, y+d10, x+d8, y+d8, x+d8, y-d8, x+d6, y-d10, x-d6, y-d10, x-d8, y-d8, x-d8, y+d8, x-d6, y+d10)),
                ('c3f', (.0549, .5765, .5020)*8))

        self.in_a.draw()
        self.in_b.draw()
        self.in_c.draw()
        self.out_a.draw()
        self.out_b.draw()
        self.out_c.draw()


    def update(self):

        self.out_a.active = self.in_a.active
        self.out_b.active = self.in_b.active
        self.out_c.active = (self.in_a.active and self.in_b.active) ^ self.in_c.active

        self.out_a.update()
        self.out_b.update()
        self.out_c.update()
            

entities = []

crosshair = pyglet.graphics.vertex_list(4,
        ('v2f', (centerx-10, centery, centerx+10, centery, centerx, centery-10, centerx, centery+10)),
        ('c3f', (1.0, 1.0, 1.0)*4))

diamond = pyglet.graphics.vertex_list(4,
        ('v2f', (centerx-8, centery, centerx, centery-8, centerx+8, centery, centerx, centery+8)),
        ('c3f', (1., 1., 1.)*4))

@window.event
def on_draw():

    window.clear()

    pyglet.graphics.draw_indexed(4, pyglet.gl.GL_TRIANGLES, [0, 1, 2, 0, 2, 3],
            ('v2f', (0, 0, window.width, 0, window.width, window.height, 0, window.height)),
            ('c3f', (.0863, .0824, .0824)*4))


    for entity in entities:
        entity.draw()


    if place_new_dr:
        for entity in place_new_entities:
            entity.draw()

        basex = -place_new_dr[0] + centerx
        basey = -place_new_dr[1] + centery
        pyglet.graphics.draw(12, pyglet.gl.GL_TRIANGLES,
                ('v2f', (basex-40, basey-40, basex-46, basey-60, basex-60, basey-46, basex+40, basey-40, basex+60, basey-46, basex+46, basey-60, basex+40, basey+40, basex+60, basey+46, basex+46, basey+60, basex-40, basey+40, basex-46, basey+60, basex-60, basey+46)),
                ('c3f', (.5, 1., .5)*12))



    if connection_start != None:
        pyglet.graphics.draw(2, pyglet.gl.GL_LINES,
                ('v2f', ((connection_start.x-mousex)*zoom_factor + centerx, (connection_start.y-mousey)*zoom_factor + centery, centerx, centery)),
                ('c3f', (1., 1., 1.)*2))


    if sens_adjust_mode:

        transform = lambda points: tuple(ichain((centerx + x*zoom_factor, centery + y*zoom_factor) for (x, y) in points))
        rectangle = lambda width, height, centerx, centery: ((centerx + width/2, centery + height/2),
                                                                         (centerx - width/2, centery + height/2),
                                                                         (centerx - width/2, centery - height/2),
                                                                         (centerx + width/2, centery - height/2))

        marker_triangles = transform(((0, -15), (7, -29), (-7, -29), (0, 15), (-7, 29), (7, 29)))

        offset = -sens_factor*100 + 100

        bars = transform(rectangle(3, 24, offset, 0) \
                         + rectangle(1, 12, offset-10, 0) \
                         + rectangle(1, 12, offset-20, 0) \
                         + rectangle(1, 12, offset-30, 0) \
                         + rectangle(1, 12, offset-40, 0) \
                         + rectangle(2, 18, offset-50, 0) \
                         + rectangle(1, 12, offset-60, 0) \
                         + rectangle(1, 12, offset-70, 0) \
                         + rectangle(1, 12, offset-80, 0) \
                         + rectangle(1, 12, offset-90, 0) \
                         + rectangle(3, 24, offset-100, 0) \
                         \
                         + rectangle(1, 12, offset+10, 0) \
                         + rectangle(1, 12, offset+20, 0) \
                         + rectangle(1, 12, offset+30, 0) \
                         + rectangle(1, 12, offset+40, 0) \
                         + rectangle(2, 18, offset+50, 0))

        bars_indices = list(ichain((0+4*i, 1+4*i, 2+4*i, 0+4*i, 2+4*i, 3+4*i) for i in range(16)))

        pyglet.graphics.draw(6, pyglet.gl.GL_TRIANGLES,
                ('v2f', marker_triangles),
                ('c3f', (1., 1., 1.)*6))

        pyglet.graphics.draw_indexed(16*4, pyglet.gl.GL_TRIANGLES, bars_indices,
                ('v2f', bars),
                ('c3f', (1., 1., 1.)*16*4))



    if drag_entity:
        diamond.draw(pyglet.gl.GL_LINE_LOOP)
    else:
        crosshair.draw(pyglet.gl.GL_LINES)

@window.event
def on_mouse_motion(x, y, dx, dy):

    global hover_entity, drag_entity, place_new_dr, place_new_entities, mousex, mousey, zoom_factor, zoom_adjust_delta, sens_factor, sens_adjust_delta

    hover_entity = None

    if zoom_adjust_mode:
        
        zoom_adjust_delta += dy
        zoom_factor = exp(zoom_adjust_delta/100.)*zoom_adjust_factor

    elif sens_adjust_mode:
        
        sens_adjust_delta += dx
        sens_factor = exp(sens_adjust_delta/100.)*sens_adjust_factor

    else:

        dx *= sens_factor
        dy *= sens_factor

        mousex += dx
        mousey += dy

        if drag_entity:
            hover_entity = drag_entity
            drag_entity.move(dx, dy)

        for entity in entities:
            entity.mouse_motion(dx, dy)


@window.event
def on_mouse_press(x, y, button, modifiers):

    global connection_start, drag_entity, place_new_dr, place_new_entities

    if drag_entity:
        drag_entity = None

    elif hover_entity:
        hover_entity.mouse_click(button)

    else:

        if button == pyglet.window.mouse.RIGHT:
            place_new_dr = [0, 0]
            place_new_entities = [Toffoli(mousex+20, mousey), MouseInput(mousex-20, mousey)]

        elif button == pyglet.window.mouse.LEFT:
            if connection_start:
                connection_start = None


@window.event
def on_mouse_drag (x, y, dx, dy, buttons, modifiers):

    global place_new_dr, mousex, mousey

    if place_new_dr:

        place_new_dr[0] += dx
        place_new_dr[1] += dy

        if (abs(place_new_dr[0]) + abs(place_new_dr[1])) >= 20:
            dr_diag = [1/sqrt(2)*(place_new_dr[0] + place_new_dr[1]), 1/sqrt(2)*(-place_new_dr[0] + place_new_dr[1])] #transform to diagonal base
            dr_diag[0] = max(min(dr_diag[0], 20/sqrt(2)), -20/sqrt(2))
            dr_diag[1] = max(min(dr_diag[1], 20/sqrt(2)), -20/sqrt(2))

            position = [1/sqrt(2)*(dr_diag[0] - dr_diag[1]), 1/sqrt(2)*(dr_diag[0] + dr_diag[1])]

        else:
            position = place_new_dr[:]

        place_new_entities[0].set_to(-position[0] +mousex +20, -position[1] +mousey)
        place_new_entities[1].set_to(-position[0] +mousex -20, -position[1] +mousey)


@window.event
def on_mouse_release (x, y, button, modifiers):

    global place_new_dr

    if place_new_dr:

        dr_diag = [1/sqrt(2)*(place_new_dr[0] + place_new_dr[1]), 1/sqrt(2)*(-place_new_dr[0] + place_new_dr[1])] #transform to diagonal base
        dr_diag[0] = max(min(dr_diag[0], 20/sqrt(2)), -20/sqrt(2))
        dr_diag[1] = max(min(dr_diag[1], 20/sqrt(2)), -20/sqrt(2))

        position = [1/sqrt(2)*(dr_diag[0] - dr_diag[1]), 1/sqrt(2)*(dr_diag[0] + dr_diag[1])]

        if position[0] >= 19:
            entities.append(Toffoli(mousex, mousey))
        elif position[0] <= -19:
            entities.append(MouseInput(mousex, mousey))
        place_new_dr = None


@window.event
def on_key_press (symbol, modifiers):

    global zoom_adjust_mode, zoom_adjust_factor, zoom_adjust_delta, sens_adjust_mode, sens_adjust_factor, sens_adjust_delta

    if symbol == pyglet.window.key.LSHIFT:

        zoom_adjust_mode = True
        zoom_adjust_factor = zoom_factor
        zoom_adjust_delta = 0

    elif symbol == pyglet.window.key.LCTRL:

        sens_adjust_mode = True
        sens_adjust_factor = sens_factor
        sens_adjust_delta = 0


@window.event
def on_key_release (symbol, modifiers):

    global zoom_adjust_mode, sens_adjust_mode

    if symbol == pyglet.window.key.LSHIFT:

        zoom_adjust_mode = False

    elif symbol == pyglet.window.key.LCTRL:

        sens_adjust_mode = False


def update (dt):

    for entity in entities:
        entity.update()

pyglet.clock.schedule_interval(update, 1/30.)
pyglet.app.run()
